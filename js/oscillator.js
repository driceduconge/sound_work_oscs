class Oscillator{//classe pour créer un oscil, bordélique
    constructor(id, portIN, portOUT){
        this.el = document.querySelector("#"+id);   //on sélectionne dans le DOM la div correspondante
        console.log(this.el);
        this.portIN = portIN;   //on crée des ports in & out
        this.portOUT = portOUT;
        this.secondsPerBeat = 60 / tempo;
        this.noteCounter = 0; // servira à faire l'arpège
        this.note = 0;
       
    }
    
    oscStart(time,freq){ //on crée un oscillateur pour chaque note, garbage-collect automatique une fois qu'il est stoppé
        var oscillator = audioCtx.createOscillator();
        const compressor = audioCtx.createDynamicsCompressor();
        compressor.threshold.setValueAtTime(0, audioCtx.currentTime);
        compressor.knee.setValueAtTime(10, audioCtx.currentTime);
        compressor.ratio.setValueAtTime(1, audioCtx.currentTime);
        compressor.attack.setValueAtTime(0.5, audioCtx.currentTime);
        compressor.release.setValueAtTime(0.25, audioCtx.currentTime);

        const distortion = audioCtx.createWaveShaper();
        distortion.curve = makeDistortionCurve(20);
        distortion.oversample = "8x";

        var filter1 = audioCtx.createBiquadFilter();
        filter1.type = "lowpass";
        filter1.frequency.value = 500;
        filter1.Q.value = 1;
        if(!firstTimePlaying){//on envoie la donnée de température si jamais la première fois
            oscillator.frequency.setValueAtTime(this.noteCalc(freq),audioCtx.currentTime);
            console.log("true now false");
        }            
        else{
            oscillator.frequency.setValueAtTime(this.noteCalc(this.note),audioCtx.currentTime);
        }
        console.log(this.type);

        oscillator.type =this.type;
        var gain = audioCtx.createGain();
        oscillator.connect(filter1);
        filter1.connect(compressor);
        compressor.connect(distortion);
        //compressor.connect(audioCtx.destination);
        distortion.connect(audioCtx.destination);

        oscillator.start(audioCtx.currentTime);
        tempo = tempss.value;

        oscillator.stop( audioCtx.currentTime + 30/tempo);
    }
    noteCalc(current_note){
        let note;
        //console.log(current_note + " " + this.noteCounter);
        if(this.noteCounter>= notesMax.value){
            console.log("updating...");
            this.noteCounter = 0;
             note = datasetReader(this.el,this);
        }
        else{ note = current_note * Math.pow((Math.pow(2,(1/12))), 2);} // maths qui font progresser fn = f° * ((2)^1/12)n
            //note = datasetReader(this.el);
        
        this.noteCounter++;
        this.note = note;
        return note;
    }
    

}
function scheduler(time, osc,timerID) {
    if(timerID != null){
        clearTimeout(timerID); // clear à chaque fois pour  infinitude
    }
    let note;
    if(!firstTimePlaying){
        console.log("first round\n\n\n\n\n");
     note = datasetReader(osc.el,this);}
    else{
        note = osc.note;
    }
    osc.oscStart(time,note);  
    tempomod = notesMax.value * 5;

    time += 20/tempo;
     timerID = setTimeout(scheduler, 100, time, osc,timerID); // timer qui s'appelle lui-même et envoie une note à chaque fois

}

function datasetReader(idx,osc){
    let retvalue;
    var datax = [].filter.call(idx.attributes, function(at) { return /^data-/.test(at.name); });
            //console.log(datax);
            Object.entries(datax).forEach(entry => {
                const [key, value] = entry;
                let name = value.name;
                let val = value.value;
                //console.log(name + " : "+ val);
            switch(name){
                case "data-temp":
                    //console.log(parseFloat(val)+getRandomArbitrary(-3,3));
                    let pastval = val;
                    retvalue = parseFloat(val)+getRandomArbitrary(-2.,2.);
                    document.querySelector("#OSC1").dataset.temp = retvalue;
                    document.querySelector("#temp").innerHTML = retvalue;
                    let variation = pastval - retvalue;
                    let vartext = variation.toString();
                    if(variation > 0){
                        document.querySelector("#variation").innerHTML ="+"+variation.toString();
                    }
                    else{
                        document.querySelector("#variation").innerHTML =variation.toString();

                    }

                    osc.type  = "sine";
                    break;
                case "data-luminosity":
                    //console.log("update lum val :::: "+val);
                    osc.type = "sine";
                
                    retvalue = val;
                    document.querySelector("#OSC2 > h2 >p").innerHTML = retvalue;
                    document.querySelector("#greyvalues").style.background = "rgb("+Math.floor((retvalue/500)*255)+","+Math.floor((retvalue/500)*255)+","+Math.floor((retvalue/500)*255)+")";
                    console.log( "rgb("+Math.floor((retvalue/500)*255)+","+Math.floor((retvalue/500)*255)+","+Math.floor((retvalue/500)*255)+")" );

                    break;
                    
                }
            })
    return retvalue
}

// mdn docz

function makeDistortionCurve(amount, cnv) {
    const k = typeof amount === "number" ? amount : 50;
    const n_samples = 8000;
    const curve = new Float32Array(n_samples);
    const deg = Math.PI / 180;
  
    for (let i = 0; i < n_samples; i++) {
      const x = (i * 2) / n_samples - 1;
      curve[i] = ((3 + k) * x * 20 * deg) / (Math.PI + k * Math.abs(x));
    }
    return curve;
  }


  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }
  
let timesincegate = 0;

function timing(){
    console.log("a");
    timesincegate++;
    document.querySelector("#lastone").innerHTML = timesincegate;
}

let interval = setInterval(timing, 1);
  function randomGates(istrue=false, inter){
    // on doit mettre à jour à chaque fois le facteur random des gates
    // ensuite on déclenche une fréquence random sur laquelle on monte ou descends avec des random +10 -10
    if(istrue){
    clearInterval(inter);
    }
    timesincegate = 0;

    //on appelle l'oscillateur en lui filant la freq


    //copie depuis la classe Oscillator
    const distortion = audioCtx.createWaveShaper();
    distortion.curve = makeDistortionCurve(200);
    distortion.oversample = "8x";

    var filter1 = audioCtx.createBiquadFilter();
    filter1.type = "highpass";
    filter1.frequency.value = 10;
    filter1.Q.value = 1;
    let val_totale = 0;
    //excerpt depuis https://netizen.org/classes/netart2/notes/web-audio/lesson.html?l=Audio%20Buffers&js=ab4.js
    const whiteBuffer = audioCtx.createBuffer(1, audioCtx.sampleRate*1, audioCtx.sampleRate)
    for (let ch=0; ch<whiteBuffer.numberOfChannels; ch++) {
        let samples = whiteBuffer.getChannelData(ch)
        let rand_timing = getRandomArbitrary(0, 50);
        val_totale = rand_timing;
        for (let s=0; s<whiteBuffer.length; s++) {samples[s] = Math.random()*rand_timing*-1;

        }
    }

    const gn = new GainNode(audioCtx, {gain:400.})

    let white = new AudioBufferSourceNode(audioCtx, {buffer:whiteBuffer})
    white.connect(distortion);
    distortion.connect(filter1);
    filter1.connect(gn);
    gn.connect(audioCtx.destination);
    
    //fin excerpt;
    let timing = audioCtx.currentTime;
    white.start(timing);
    
    white.stop(timing + getRandomArbitrary(0, 1));
    //set TimeOutrandom
    let interval = setInterval(timing, 1);

    setTimeout(randomGates, getRandomArbitrary(1, 30000),true,interval);

}