
const audioCtx = new AudioContext({sampleRate: 8000,});
const analyser = audioCtx.createAnalyser();
const myDataArray = new Float32Array(analyser.frequencyBinCount);

var temp;
let tempo = 100; //le tempo c'est une valeur que je capte pas ici elle n'est pas réelle.
let notesMax = document.querySelector("#notes_range");
let tempss = document.querySelector("#tempo_range")
// code pour récupérer données météo avec openweathermap

function weatherBalloon( cityID ) {
    var key = '{yourkey}';
    fetch('https://api.openweathermap.org/data/2.5/weather?id=2826258&appid=f7ce19c1d25ffb5ebdb904f6637482d9')  
    .then(function(resp) { return resp.json() }) // Convert data to json
    .then(function(data) {
      temp = data["main"]["temp"];
      document.querySelector("#OSC1").dataset.temp = temp;
      console.log(temp);
    })
    .catch(function() {
      // catch any errors
    });
  }
  
  window.onload = function() {
    weatherBalloon( 6167865 );
  }

// webcam code from mdn + stack
const constraints = {
  audio: false,
  video: { width: 100, height: 100 }
};
const osc2 = document.querySelector("#OSC2");
const ctx = canvas.getContext('2d');  
const video = document.querySelector('video');
updateCanvas();

  function updateCanvas(){
    navigator.mediaDevices.getUserMedia(constraints)

    .then((mediaStream) => {
      video.srcObject = mediaStream;
      updateCanvas();
 
    video.onloadedmetadata = () => {
        ctx.drawImage(video, 0, 0, 16, 12);
        let frame = ctx.getImageData(0, 0, 16, 12);
    let data = frame.data;
    let brightness = 0;
    for (let i = 0; i < data.length; i += 4) {
      let red = data[i + 0];
      let green = data[i + 1];
      let blue = data[i + 2];
       brightness += (red+green+blue)/3;
    }
    brightness /=(data.length/4);
    osc2.dataset.luminosity = brightness*2.;
    //console.log("brightness : "+brightness);
      setTimeout(updateCanvas,100);
    };
  })
  }

var firstTimePlaying = true;


//initiate canvas first 

var dpr = window.devicePixelRatio || 1;
let canvas2 = document.getElementById("plotter_temp");
var rect = canvas2.getBoundingClientRect();
let ctxx = canvas2.getContext("2d");
ctxx.scale(dpr, dpr);
canvas2.width = rect.width * dpr;
canvas2.height = rect.height * dpr;



let osc1click = true;
document.querySelector("#OSC1").onclick = (event) => {
  if(osc1click){
    var nextNoteTime = audioCtx.currentTime;
    var nextNoteTime = audioCtx.currentTime; const osci_1 = new Oscillator("OSC1",0001,1000); //click nécessaire pour créer l'audiocontext
    osci_1.el.dataset.playing = true;
    let timerID1 = null;
    drawPlot(canvas2,ctxx,"temp");
    scheduler(nextNoteTime,osci_1,timerID1); // on lance la machine
    osc1click = false;
  }
};

let osc2click = true;
document.querySelector("#OSC2").onclick = (event) => {
  if(osc2click){
    var nextNoteTime = audioCtx.currentTime;
    var nextNoteTime2 = audioCtx.currentTime; const osci_2 = new Oscillator("OSC2",0001,1000); //click nécessaire pour créer l'audiocontext
    osci_2.el.dataset.playing = true;
    let timerID2 = null;
    scheduler(nextNoteTime2,osci_2,timerID2); // on lance la machine
    osc2click = false;
  }
};


let canvas3 = document.getElementById("visu_gates");
var rect2 = canvas3.getBoundingClientRect();
let ctxxx = canvas3.getContext("2d");
ctxxx.scale(dpr, dpr);
canvas3.width = rect2.width * dpr;
canvas3.height = rect2.height * dpr;

document.querySelector("#OSC3").onclick = (event) =>{
  let listing = new Array(561);
  drawingRadio(canvas3, ctxxx, listing);
  randomGates();
}
// stack overflow 

function map_range(value, low1, high1, low2, high2) {
  return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}